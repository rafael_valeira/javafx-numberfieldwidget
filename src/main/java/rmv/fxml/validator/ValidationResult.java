package rmv.fxml.validator;

import java.util.Objects;

/**
 * Stores validation result from {@link Validator}s.
 */
public class ValidationResult {

	private Boolean isValid;

	private String message;

	public ValidationResult( Boolean isValid ) {

		this( isValid, "" );
	}

	public ValidationResult( Boolean isValid, String message ) {

		this.isValid = isValid;
		this.message = message;
	}

	/**
	 * Whether the validation passed or not.
	 *
	 * @return Boolean
	 */
	public Boolean isValid() {

		return this.isValid;
	}

	/**
	 * Set if the validation passed or not.
	 */
	public void setIsValid( Boolean isValid ) {

		this.isValid = isValid;
	}

	/**
	 * The validation message.
	 *
	 * @return String
	 */
	public String getMessage() {

		return this.message;
	}

	/**
	 * Sets the validation message.
	 *
	 * @param message
	 */
	public void setMessage( String message ) {

		this.message = message;
	}

	@Override
	public int hashCode() {

		return Objects.hash( this.isValid, this.message );
	}

	@Override
	public boolean equals( Object obj ) {

		if( obj == null ) {

			return false;
		}

		if( this.getClass() != obj.getClass() ) {

			return false;
		}

		if( this == obj ) {

			return true;
		}

		ValidationResult other = (ValidationResult) obj;

		return Objects.equals( this.getMessage(), other.getMessage() )
				&& Objects.equals( this.isValid(), other.isValid );
	}
}
