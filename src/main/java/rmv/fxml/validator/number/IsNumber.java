package rmv.fxml.validator.number;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

import rmv.fxml.validator.ValidationResult;
import rmv.fxml.validator.Validator;

/**
 * {@link Validator} that checks if a {@link String} can be converted to a number.
 */
public class IsNumber implements Validator<String> {

	@Override
	public ValidationResult validate( String value ) {

		ValidationResult result = new ValidationResult( TRUE );

		try {

			Float.valueOf( value );
		} catch( NumberFormatException | NullPointerException e ) {

			result.setIsValid( FALSE );
			result.setMessage( this.createMessage( value ) );
		}

		return result;
	}

	private String createMessage( String value ) {

		return new StringBuilder( value == null ? "null" : value ).append( " is not a number." ).toString();
	}
}
