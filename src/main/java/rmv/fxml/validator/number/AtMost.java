package rmv.fxml.validator.number;

import java.math.BigDecimal;

import rmv.fxml.validator.Validator;

public class AtMost extends NumberComparisonValidator implements Validator<String> {

	public AtMost( String maximum ) {

		super( maximum );
	}

	@Override
	protected boolean compare( BigDecimal maximum, BigDecimal toCompare ) {

		return maximum.compareTo( toCompare ) >= 0;
	}

	@Override
	protected String comparisonMessage() {

		return " is bigger then ";
	}
}
