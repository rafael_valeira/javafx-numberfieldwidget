package rmv.fxml.validator.number;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

import rmv.fxml.validator.ValidationResult;
import rmv.fxml.validator.Validator;

/**
 * {@link Validator} that checks if a {@link String} can be converted to an {@link Integer}.
 */
public class IsInteger implements Validator<String> {

	@Override
	public ValidationResult validate( String value ) {

		ValidationResult result = new ValidationResult( TRUE );

		try {

			Integer.valueOf( value );
		} catch( NumberFormatException | NullPointerException e ) {

			result.setIsValid( FALSE );
			result.setMessage( this.createMessage( value ) );
		}

		return result;
	}

	private String createMessage( String value ) {

		String message = new StringBuilder( value == null ? "null" : value ).append( " is not an integer." ).toString();
		return message;
	}
}
