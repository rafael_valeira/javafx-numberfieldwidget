package rmv.fxml.validator.number;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

import java.math.BigDecimal;

import rmv.fxml.validator.ValidationResult;
import rmv.fxml.validator.Validator;

/**
 * Helper abstract {@link Validator} used for number comparator validators.
 */
abstract class NumberComparisonValidator implements Validator<String> {

	private BigDecimal baseNumber;

	private String originalValue;

	private int scale;

	public NumberComparisonValidator( String baseValue ) {

		this.scale = baseValue.substring( baseValue.indexOf( "." ) + 1 ).length();
		this.baseNumber = this.createNumber( baseValue );
		this.originalValue = baseValue;
	}

	@Override
	public ValidationResult validate( String value ) {

		Boolean isValid = TRUE;
		String message = "";
		BigDecimal toCompare = this.createNumber( value );

		if( !this.compare( this.baseNumber, toCompare ) ) {

			isValid = FALSE;
			message = this.createMessage( value, this.comparisonMessage() );
		}

		return new ValidationResult( isValid, message );
	}

	/**
	 * Executes the comparison.
	 *
	 * @param baseNumber
	 *            The base number to be compared against.
	 * @param toCompare
	 *            The number that is going to be compared.
	 *
	 * @return Boolean
	 */
	protected abstract boolean compare( BigDecimal baseNumber, BigDecimal toCompare );

	/**
	 * A message that specifies which comparison was made. This will be used when creating
	 * {@link ValidationResult}'s message. <br>
	 * The message returned by this method should inform why the comparison is wrong. <br>
	 * For example, if the compared value should be equals to the base value, the message returned by this method
	 * should be something like ' is not equal to ', so the {@link ValidationResult}'s message would say
	 * 'baseNumber is not equal to toCompareValue', where baseNumber and toCompareValue would be substituted by the
	 * actual numbers.
	 *
	 * @return The message.
	 */
	protected abstract String comparisonMessage();

	/**
	 * Creates a {@link BigDecimal}, where the scale will be set to the same
	 * as the valueToCheck class property.
	 *
	 * @param value
	 *
	 * @return The converted number.
	 */
	private BigDecimal createNumber( String value ) {

		BigDecimal bd = new BigDecimal( value );
		bd = bd.setScale( this.scale, BigDecimal.ROUND_HALF_EVEN );

		return bd;
	}

	/**
	 * Creates a message, used when validation fails.
	 *
	 * @param value
	 *            The value that failed the comparison.
	 * @param comparison
	 *            Message specific to the comparison.
	 *
	 * @return The created message.
	 */
	private String createMessage( String value, String comparison ) {

		return new StringBuilder( value ).append( comparison ).append( this.originalValue ).toString();
	}
}
