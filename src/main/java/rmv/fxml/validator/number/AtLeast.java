package rmv.fxml.validator.number;

import java.math.BigDecimal;

import rmv.fxml.validator.Validator;

/**
 * {@link Validator} that checks whether a number is bigger then predefined {@link Number}.
 */
public class AtLeast extends NumberComparisonValidator implements Validator<String> {

	public AtLeast( String minimum ) {

		super( minimum );
	}

	@Override
	protected boolean compare( BigDecimal minimum, BigDecimal toCompare ) {

		return minimum.compareTo( toCompare ) <= 0;
	}

	@Override
	protected String comparisonMessage() {

		return " is smaller then ";
	}
}
