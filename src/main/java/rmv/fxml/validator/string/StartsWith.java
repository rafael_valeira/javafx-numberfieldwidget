package rmv.fxml.validator.string;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

import rmv.fxml.validator.ValidationResult;
import rmv.fxml.validator.Validator;

/**
 * {@link Validator} that checks whether an {@link String} starts width another predefined {@link String}.
 */
public class StartsWith implements Validator<String> {

	private String start;

	public StartsWith( String start ) {

		if( start == null ) {

			throw new IllegalArgumentException( "null is not a valid prefix." );
		}

		this.start = start;
	}

	public ValidationResult validate( String value ) {

		ValidationResult result = new ValidationResult( TRUE );

		if( value == null || !value.startsWith( this.start ) ) {
			result.setIsValid( FALSE );
			result.setMessage( this.message( value ) );
		}

		return result;
	}

	private String message( String value ) {

		String message = new StringBuilder( "\"" ).append( value ).append( "\" does not start with \"" )
				.append( this.start ).append( "\"." ).toString();
		return message;
	}
}
