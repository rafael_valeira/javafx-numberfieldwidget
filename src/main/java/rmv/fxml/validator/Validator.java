package rmv.fxml.validator;

public interface Validator<T> {

	/**
	 * Performs validation.
	 *
	 * @param value
	 *
	 * @return {@link Boolean}
	 */
	ValidationResult validate( T value );
}
