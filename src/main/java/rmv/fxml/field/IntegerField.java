package rmv.fxml.field;

import java.io.IOException;
import java.net.URL;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextField;
import rmv.fxml.validator.Validator;
import rmv.fxml.validator.number.IsNumber;

/**
 * IntegerField JavaFX component.
 * A {@link TextField} specialization that allows only {@link Integer} numbers as input.
 */
public class IntegerField extends TextField implements ChangeListener<String> {

	// ****************************
	// * Initialization variables *
	// ****************************
	private boolean initialized = false;

	// *********************
	// * Control variables *
	// *********************
	private static final String view = "Node.fxml";

	// *****************************
	// * Component's FX properties *
	// *****************************
	/**
	 * Informs whether this {@link IntegerField} allows negative numbers or not.
	 */
	private BooleanProperty allowNegative = new SimpleBooleanProperty( true );

	public final boolean getAllowNegative() {

		return this.allowNegative.get();
	}

	public final void setAllowNegative( boolean value ) {

		this.allowNegative.set( value );
	}

	public BooleanProperty allowNegativeProperty() {

		return this.allowNegative;
	}

	/**
	 * Informs whether this {@link IntegerField} should put a limitation to the lowest possible number.
	 */
	private BooleanProperty limitMinimum = new SimpleBooleanProperty( false );

	public final boolean getLimitMinimum() {

		return this.limitMinimum.get();
	}

	public final void setLimitMinimum( boolean value ) {

		this.limitMinimum.set( value );
	}

	public BooleanProperty limitMinimumProperty() {

		return this.limitMinimum;
	}

	/**
	 * The minimum value allowed for this {@link IntegerField}.
	 */
	private IntegerProperty minimumValue = new SimpleIntegerProperty();

	public final Integer getMinimumValue() {

		return this.minimumValue.get();
	}

	public final void setMinimumValue( Integer value ) {

		this.minimumValue.set( value );
	};

	public IntegerProperty getMinimumValueProperty() {

		return this.minimumValue;
	};

	/**
	 * Informs whether this {@link IntegerField} should put a limitation to the highest possible number.
	 */
	private BooleanProperty limitMaximum = new SimpleBooleanProperty( false );

	public final boolean getLimitMaximum() {

		return this.limitMaximum.get();
	}

	public final void setLimitMaximum( boolean value ) {

		this.limitMaximum.set( value );
	}

	public BooleanProperty limitMaximumProperty() {

		return this.limitMaximum;
	}

	/**
	 * The maximum value allowed for this {@link IntegerField}.
	 */
	private IntegerProperty maximumValue = new SimpleIntegerProperty();

	public final Integer getMaximumValue() {

		return this.maximumValue.get();
	}

	public final void setMaximumValue( Integer value ) {

		this.maximumValue.set( value );
	}

	public IntegerProperty getMaximumValueProperty() {

		return this.maximumValue;
	}

	private ObjectProperty<FieldType> numberType;

	public final FieldType getNumberType() {

		return this.numberType.get();
	}

	public final void setNumberType( FieldType type ) {

		this.numberType.set( type );
	}

	public ObjectProperty<FieldType> numberType() {

		if( this.numberType == null ) {

			this.numberType = new SimpleObjectProperty<FieldType>() {

				@Override
				public String getName() {

					return "numbertype";
				}
			};
		}

		return this.numberType;
	}

	// **************************
	// * Initialization methods *
	// **************************
	@Override
	protected void layoutChildren() {

		if( !this.initialized ) {
			this.init();
		}

		super.layoutChildren();
	}

	/**
	 * Initializes this component, if it is not already initialized.
	 */
	private void init() {

		this.initialized = true;

		try {

			this.setValue( 0 );

			URL resource = this.getClass().getClassLoader().getResource( IntegerField.view );

			FXMLLoader loader = new FXMLLoader( resource );
			loader.setRoot( this );
			loader.setController( this );

			loader.load();

		} catch( IOException exception ) {
			throw new RuntimeException( exception );
		}

		this.textProperty().addListener( this );
	}

	// *******************************
	// * Component behavior methods. *
	// *******************************
	@Override
	public void changed( ObservableValue<? extends String> observable, String oldValue, String newValue ) {

		Validator<String> isInteger = new IsNumber();
		if( !isInteger.validate( newValue ).isValid() ) {

			this.setText( oldValue );
		}
	}

	// ********************************
	// * Component properties methods *
	// ********************************
	public Integer getValue() {

		String textValue = this.getText();
		return Integer.valueOf( textValue );
	}

	public void setValue( Integer value ) {

		this.setText( value.toString() );
	}
}
