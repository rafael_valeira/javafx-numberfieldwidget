package rmv.fxml.validator.string;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.junit.ExpectedException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;

/**
 * Tests for {@link StartsWith}.
 */
@RunWith( JUnitParamsRunner.class)
public class StartsWithTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	@Parameters( method = "validValues")
	@TestCaseName( "should inform that '{0}' starts with '{1}'")
	public void shouldValidateWhenStringStartsWithRequiredString( String value, String start ) {

		assertThat( new StartsWith( start ).validate( value ).isValid(), is( TRUE ) );
	}

	@Test
	@Parameters( method = "invalidValues")
	@TestCaseName( "should inform that '{0}' does not start with '{1}'")
	public void shouldInformThatStringDoesNotStartWithRequiredString( String value, String start ) {

		assertThat( new StartsWith( start ).validate( value ).isValid(), is( FALSE ) );
	}

	@Test
	@Parameters( method = "invalidValues")
	@TestCaseName( "should create message when '{0}' does not start with '{1}'")
	public void shouldCreateMessage( String value, String start ) {

		String message = new StringBuilder( "\"" ).append( value ).append( "\" does not start with \"" ).append( start )
				.append( "\"." ).toString();
		assertThat( new StartsWith( start ).validate( value ).getMessage(), is( message ) );
	}

	@Test
	public void shouldInformThatNullDoesNotStartWithAnything() {

		assertThat( new StartsWith( "" ).validate( null ).isValid(), is( FALSE ) );
	}

	@Test
	public void cannotCreateValidatorForNullStarterString() {

		this.thrown.expect( IllegalArgumentException.class );
		this.thrown.expectMessage( "null is not a valid prefix." );

		new StartsWith( null );
	}

	public List<Object[]> validValues() {

		return Arrays.asList( new Object[] { "-", "-" }, new Object[] { "-a", "-" }, new Object[] { "@12345", "@12" } );
	}

	public List<Object[]> invalidValues() {

		return Arrays.asList( new Object[] { "  -", "-" }, new Object[] { "a-", "-" }, new Object[] { "@1345", "@12" },
				new Object[] { null, "" } );
	}
}
