package rmv.fxml.validator.number;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;

/**
 * Tests for {@link IsNumber}.
 */
@RunWith( JUnitParamsRunner.class)
public class IsNumberTest {

	private IsNumber validator;

	@Before
	public void setUp() {

		this.validator = new IsNumber();
	}

	@Test
	@Parameters( method = "numbers")
	@TestCaseName( "should inform that \"{0}\" is a number")
	public void shouldValidateNumber( String value ) {

		assertThat( this.validator.validate( value ).isValid(), is( TRUE ) );
	}

	@Test
	@Parameters( method = "notNumbers")
	@TestCaseName( "should inform that \"{0}\" is not a number")
	public void shouldInformThatStringIsNotANumber( String value ) {

		assertThat( this.validator.validate( value ).isValid(), is( FALSE ) );
	}

	@Test
	@Parameters( method = "notNumbers")
	@TestCaseName( "should create message when \"{0}\" is not a number")
	public void shouldCreateMessageWhenValidationFails( String value ) {

		String message = new StringBuilder( value == null ? "null" : value ).append( " is not a number." ).toString();
		assertThat( this.validator.validate( value ).getMessage(), is( message ) );
	}

	public List<String> numbers() {

		return asList( "5", "500", "5.0", "5.5", "-5", "05", ".05", "   50", "5." );
	}

	public List<String> notNumbers() {

		return asList( "", "  ", "a", "5a", "5a5", "-5a", "5!", ".05a", "0x01", "5000000a", "0.0000001000a", null );
	}
}
