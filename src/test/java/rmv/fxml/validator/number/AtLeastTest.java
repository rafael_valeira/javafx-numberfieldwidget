package rmv.fxml.validator.number;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;

/**
 * Tests for {@link AtLeast}.
 */
@RunWith( JUnitParamsRunner.class)
public class AtLeastTest {

	@Test
	@Parameters( method = "validValues")
	@TestCaseName( "should inform that {0} is at least {1}.")
	public void shouldPassValidation( String value, String minimum ) {

		assertThat( new AtLeast( minimum ).validate( value ).isValid(), is( TRUE ) );
	}

	@Test
	@Parameters( method = "invalidValues")
	@TestCaseName( "should inform that {0} is smaller then {1}.")
	public void shouldFailValidation( String value, String toCheck ) {

		assertThat( new AtLeast( toCheck ).validate( value ).isValid(), is( FALSE ) );
	}

	@Test
	@Parameters( method = "invalidValues")
	@TestCaseName( "should create message when {0} is smaller then {1}.")
	public void shouldCreateFailValidationMessage( String value, String toCheck ) {

		String message = new StringBuilder( value ).append( " is smaller then " ).append( toCheck ).toString();
		assertThat( new AtLeast( toCheck ).validate( value ).getMessage(), is( message ) );
	}

	public List<Object[]> validValues() {

		return Arrays.asList( new Object[] { "51", "50" }, new Object[] { "-12", "-13" },
				new Object[] { "13", "12.99" }, new Object[] { "-13", "-13" }, new Object[] { "50", "50" },
				new Object[] { "50.0101", "50.01" } );
	}

	public List<Object[]> invalidValues() {

		return Arrays.asList( new Object[] { "49", "50" }, new Object[] { "12", "12.01" },
				new Object[] { "50.001", "50.002" } );
	}
}
