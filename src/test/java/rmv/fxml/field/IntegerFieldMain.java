package rmv.fxml.field;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main JavaFX file to test {@link IntegerField}.
 */
public class IntegerFieldMain extends Application {

	@Override
	public void start( Stage primaryStage ) {

		Scene scene = new Scene( new IntegerField() );
		primaryStage.setScene( scene );
		primaryStage.show();
	}

	public static void main( String[] args ) {

		launch( args );
	}
}
